<?php

namespace App\Imports;
use App\Bulk;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
class BulkImport implements ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Bulk([
            'epc'     => $row['epc'],
            'component'    => $row['component'],
            'product_nr'    => $row['product_nr'],
            'manufacturing'    => date('Y-m-d', strtotime($row['manufacturing'])),
            'lot_number'    => $row['lot_number'],
            'compound'    => $row['compound'],
            'notes'    => $row['notes']
        ]);
    }

    public function rules(): array
    {
        return [
            'epc' => 'unique:tags,epc|required|max:100',
            'component'    => 'required|max:10',
            'product_nr'    => 'required|max:10',
            'manufacturing'    => 'date|required',
            'lot_number'    => 'required|max:10',
            'compound'    => 'required|max:10',
            'notes'    => 'required|max:100'
        ];
    }
}
