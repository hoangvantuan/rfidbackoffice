<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class CreateUser extends Model
{
    protected $table = 'users';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'insert', 'pwd','status', 'update_ts',
    ];


    static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
            $model->update_ts = Date::now();
        });
    }
}
