<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class Traces extends Model
{
    protected $table = 'traces';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'epc', 'operation','operation_ts','location','from_user'
    ];

    static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
            $model->operation_ts = Date::now();
        });
    }
}
