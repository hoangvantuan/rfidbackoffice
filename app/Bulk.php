<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class Bulk extends Model
{
    protected $table = 'tags';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'epc', 'component', 'product_nr', 'manufacturing', 'lot_number', 'compound', 'notes'
    ];

    static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
            $model->update_ts = Date::now();
        });
    }
}
