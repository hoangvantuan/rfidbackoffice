<?php

namespace App\Http\Controllers;

use App\CreateUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class RfidUserDeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $users = DB::table('users')->where('id', '=', $id)->get();
        return view('users.delete_users',['users'=>$users]);
    }

    public function delete(Request $request , $id)
    {
        $del = DB::table('users')->where('id', '=', $id)->delete();
        return redirect()->route('users');
    }
}
