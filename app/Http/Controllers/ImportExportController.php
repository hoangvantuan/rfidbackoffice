<?php

namespace App\Http\Controllers;
use App\Imports\BulkImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
class ImportExportController extends Controller
{
    /**
     *
     */
    public function importExportView()
    {
        return view('epc.bulk-import');
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);

        Excel::import(new BulkImport,request()->file('file'));

        return redirect()->route('epc');
    }
}
