<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function username()
    {
        return 'username';
    }

    protected function validateLogin(Request $request)
    {
//        User::first()->update(['username' => 'lxc@gmail.com', 'pwd'=> Hash::make('password')]);
        $request->validate([
            $this->username() => 'required|string',
            'pwd' => 'required|string',
        ]);
    }

    protected function credentials(Request $request)
    {
        return [
            'username' => $request->username,
            'password' => $request->pwd,
            'status' => "ad",
        ];
    }

}
