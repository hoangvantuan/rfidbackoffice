<?php

namespace App\Http\Controllers;

use App\Epc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class EpcController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $epc = DB::table('tags')->get();
        return view('epc.list_epc', ['epc' => $epc]);
    }

    public function create()
    {
        return view('epc.create_epc');
    }

    public function store(Request $request)
    {
        $request->validate([
            'epc' => 'required|unique:tags|max:100',
            'component' => 'required|max:10',
            'product_nr' => 'required|max:10',
            'manufacturing' => 'required',
            'lot_number' => 'required|max:10',
            'compound' => 'required|max:10',
            'notes' => 'required|max:100'
        ]);

        $news_epc = new Epc;
        $news_epc->epc = $request->epc;
        $news_epc->component = $request->component;
        $news_epc->product_nr =  $request->product_nr;
        $news_epc->manufacturing = date('Y-m-d', strtotime($request->manufacturing));
        $news_epc->lot_number = $request->lot_number;
        $news_epc->compound = $request->compound;
        $news_epc->notes = $request->notes;
        $news_epc->save();

        return redirect()->route('epc');
    }
}
