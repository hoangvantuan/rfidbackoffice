<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EpcByLocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function EpcByLocation(Request $request)
    {

        $query = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.epc as show_epc,
                locations.location as show_location,
                users.username as show_username,
                DATE(operation_ts) as date_flt')
            );

        if(request()->ajax())
        {
            $output = '';
            $filter_epc_by_location = $request->filter_epc_by_location;

            if($filter_epc_by_location !="")
            {
                $result = $query->where('traces.location', '=', $filter_epc_by_location);
            }

            if($filter_epc_by_location =="")
            {
                $result = $query;
            }

            $traces = $result->orderByDesc('traces.id')->get();

            if ($traces)
            {
                foreach ($traces as $key => $trace)
                {
                    $output .= '
                        <tr>
                            <td>' . $trace->show_epc . '</td>
                            <td>' . $trace->operation . '</td>
                            <td>' . $trace->operation_ts. '</td>
                            <td>' . $trace->show_location . '</td>
                            <td>' . $trace->show_username . '</td>
                        </tr>';
                }
            }

            return Response($output);
        }

        $traces = $query->get();

        $locations_traces = DB::table('locations')
            ->select('locations.*')
            ->get();

        return view('reports.epc-by-location',['locations_traces' => $locations_traces,'traces' => $traces]);
    }
}
