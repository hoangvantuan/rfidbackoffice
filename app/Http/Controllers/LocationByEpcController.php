<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationByEpcController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function LocationByEpc()
    {
        $traces = DB::table('traces')
            ->select('location')
            ->get();
        $location = array();

        foreach ($traces as $trace) {
            $location[] = $trace->location;
        }

        $get_locations = DB::table('locations')
            ->select('locations.*')
            ->whereIn('id', $location)
            ->get();

        return view('reports.location-by-epc', ['get_locations' => $get_locations]);
    }
}

