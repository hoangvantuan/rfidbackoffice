<?php

namespace App\Http\Controllers;

use App\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $locations = DB::table('locations')->get();
        return view('locations.list_locations', ['locations' => $locations]);
    }

    public function create()
    {
        return view('locations.create_locations');
    }

    public function store(Request $request)
    {
        $request->validate([
            'location' => 'required|max:255',
            'location_type' => 'required|max:10',
        ]);
        $news_locations = new Locations;
        $news_locations->location = $request->location;
        $news_locations->location_type = $request->location_type;
        $news_locations->save();
        return redirect()->route('locations');
    }
}
