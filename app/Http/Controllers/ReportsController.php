<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Constraint\Count;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('reports.list');
    }

    public function inbound()
    {
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.epc as show_epc,
                locations.location as show_location,
                users.username as show_username,
                DATE(operation_ts) as date_flt'))
            ->where('operation','=','IN')
            ->whereDate('operation_ts', '>',  Carbon::now()->subDays(15))
            ->get();
        return view('reports.inbound', ['traces' => $traces]);
    }

    public function outbound()
    {
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.epc as show_epc,
                locations.location as show_location,
                users.username as show_username,
                DATE(operation_ts) as date_flt'))
            ->where('operation','=','OUT')
            ->whereDate('operation_ts', '>',  Carbon::now()->subDays(15))
            ->get();
        return view('reports.outbound', ['traces' => $traces]);
    }

    public function LocationInbound()
    {
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select(DB::raw('traces.* ,
                COUNT(traces.location) as count,tags.epc as show_epc,
                locations.location as show_location,
                users.username as show_username,
                DATE(operation_ts) as date_flt'))
            ->where('operation','=','IN')
            ->groupBy('traces.location')
            ->get();
       return view('reports.location-inbound', ['traces' => $traces]);
    }

    public function LocationOutbound()
    {
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select(DB::raw('traces.* ,
                COUNT(traces.location) as count,tags.epc as show_epc,
                locations.location as show_location,
                users.username as show_username,
                DATE(operation_ts) as date_flt'))
            ->where('operation','=','OUT')
            ->groupBy('traces.location')
            ->get();
        return view('reports.location-outbound', ['traces' => $traces]);
    }
}
