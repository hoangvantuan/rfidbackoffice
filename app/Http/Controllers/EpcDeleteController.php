<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EpcDeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $epc = DB::table('tags')->where('id', '=', $id)->get();
        return view('epc.delete_epc',['epc'=>$epc]);
    }

    public function delete(Request $request , $id)
    {
        $del = DB::table('tags')->where('id', '=', $id)->delete();
        return redirect()->route('epc');
    }
}
