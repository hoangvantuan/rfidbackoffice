<?php

namespace App\Http\Controllers;

use App\CreateUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class RfidUserUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $users = DB::table('users')->where('id', '=', $id)->get();
        return view('users.edit_users',['users'=>$users]);
    }

    public function getDirty(Request $request , $id )
    {
        $epc = DB::table('users')
            ->select('username','pwd','status')
            ->where('id', '=', $id)->first();
        $update_epc = $request->all('username','pwd','status');
        $epc = (array) $epc;
        $array_change = array_diff($epc, $update_epc);
        if(!empty($array_change))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request , $id)
    {
        $request->validate([
            'username' => [
                'required',
                'max:32',
                Rule::unique('users')->ignore($id),
            ],
            'status' => 'required|max:3',
            'pwd' => 'required|min:6'
        ]);
        $username = $request->username;
        $pwd = $request->pwd;
        $status = $request->status;
        if($this->getDirty($request, $id))
        {
            DB::table('users')
                ->where('id', $id)
                ->update(['username' => $username ,'pwd' => $pwd , 'status' => $status]);
            return redirect()->route('users');
        }
        else
        {
            return redirect()->route('users.edit', ['id'=>$id])->with('failed',"No fields were modifieds");
        }
    }
}
