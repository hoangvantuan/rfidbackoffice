<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\CreateUser;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\Rule;

class RfidUser extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $list_user = DB::table('users')->where('status', '<>', 'ad')->get();
        return view('users.list_user', ['users' => $list_user]);
    }

    public function create()
    {
        return view('users.create_users');
    }

    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:users|max:32',
            'status' => 'required|max:3',
            'pwd' => 'required|min:6'
        ]);
        $news_user = new CreateUser;
        $news_user->username = $request->username;
        $news_user->pwd = $request->pwd;
        $news_user->status = $request->status;
        $news_user->save();
        return redirect()->route('users');
    }
}
