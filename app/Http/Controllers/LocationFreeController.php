<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationFreeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function FreeLocation()
    {
        $traces = DB::table('traces')
            ->select('location')
            ->get();
        $location = array();
        foreach ($traces as $trace){
            $location[]= $trace->location;
        }
        $free_locations = DB::table('locations')
            ->select('locations.*')
            ->whereNotIn('id', $location)
            ->get();
        return view('reports.location-free',['free_locations' => $free_locations]);
    }
}
