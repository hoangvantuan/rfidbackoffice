<?php

namespace App\Http\Controllers;

use App\Traces;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TracesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.epc as show_epc,
                    locations.location as show_location,
                    users.username as show_username')
            )
            ->get();
        return view('traces.list_traces', ['traces' => $traces]);
    }

    public function create()
    {
        $epc = DB::table('tags')->get();
        $users = DB::table('users')->where('status', '<>', 'ad')->get();
        $locations = DB::table('locations')->get();
        return view('traces.create_traces',['epc' => $epc,'users' => $users,'locations' => $locations]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'epc' => 'required',
            'operation' => 'required',
            'location' => 'required',
            'from_user' => 'required'
        ]);

        $new_traces = new Traces();
        $new_traces->epc = $request->epc;
        $new_traces->operation = $request->operation;
        $new_traces->location = $request->location;
        $new_traces->from_user = $request->from_user;
        $new_traces->save();

        return redirect()->route('traces');
    }
}
