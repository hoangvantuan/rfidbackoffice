<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class EpcUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $epc = DB::table('tags')->where('id', '=', $id)->get();
        return view('epc.edit_epc',['epc'=>$epc]);
    }

    public function getDirty(Request $request , $id )
    {
        $epc = DB::table('tags')
            ->select('epc','component','product_nr',
                DB::raw('DATE_FORMAT(manufacturing,"%d-%m-%Y") as manufacturing'),'lot_number','compound','notes')
            ->where('id', '=', $id)->first();
        $update_epc = $request->all('epc','component','product_nr','manufacturing','lot_number','compound','notes');
        $epc = (array) $epc;
        $array_change = array_diff($epc, $update_epc);

        if(!empty($array_change))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request , $id)
    {
        $request->validate([
            'epc' => 'required|max:100|unique:tags,epc,'.$request->epc.',epc',
            'component' => 'required|max:10',
            'product_nr' => 'required|max:10',
            'manufacturing' => 'required',
            'lot_number' => 'required|max:10',
            'compound' => 'required|max:10',
            'notes' => 'required|max:100'
        ]);

        $epc = $request->epc;
        $component  = $request->component;
        $product_nr  = $request->product_nr;
        $manufacturing  = date('Y-m-d', strtotime($request->manufacturing));
        $lot_number  = $request->lot_number;
        $compound  = $request->compound;
        $notes  = $request->notes;

        if ($this->getDirty($request , $id))
        {
            DB::table('tags')
                ->where('id', $id)
                ->update([
                    'epc' => $epc,
                    'component' => $component,
                    'product_nr' => $product_nr,
                    'manufacturing' => $manufacturing,
                    'lot_number' => $lot_number,
                    'compound' => $compound,
                    'notes' => $notes
                ]);
            return redirect()->route('epc');
        }
        else
        {
            return redirect()->route('epc.edit', ['id'=>$id])->with('failed',"No fields were modifieds");
        }
    }
}
