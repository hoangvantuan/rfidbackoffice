<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TracesDeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $epc = DB::table('tags')->get();
        $users = DB::table('users')->where('status', '<>', 'ad')->get();
        $locations = DB::table('locations')->get();
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.id as tags_id,
                    locations.id as location_id,
                    users.id as user_id')
            )
            ->where('traces.id', '=', $id)
            ->get();
        return view('traces.delete_traces', ['traces' => $traces,'epc' => $epc,'users' => $users,'locations' => $locations]);
    }

    public function delete($id)
    {
        DB::table('traces')->where('id', '=', $id)->delete();
        return redirect()->route('traces');
    }
}
