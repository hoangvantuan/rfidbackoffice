<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TracesSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request)
    {
        $query = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select(DB::raw('traces.*,
                    tags.epc as show_epc,
                    locations.location as show_location,
                    users.username as show_username,
                    DATE(operation_ts) as date_flt'));

        if(request()->ajax())
        {
            $output = '';
            $filter_operation_ts = $request->filter_operation_ts;
            $filter_operation = $request->filter_operation;

            if($filter_operation_ts !="")
            {
                $result =  $query->whereDate('operation_ts', '=', date('Y-m-d',strtotime($filter_operation_ts)));
            }

            if ($filter_operation !="")
            {
                $result =  $query->where('operation', 'LIKE', '%' . $filter_operation . '%');
            }

            if($filter_operation_ts =="" && $filter_operation =="")
            {
                $result =  $query;
            }

            $traces = $result->orderByDesc('locations.location')->get();

            if ($traces)
            {
                foreach ($traces as $key => $trace)
                {
                    $output .= '
                        <tr>
                            <td>' . $trace->show_epc . '</td>
                            <td>' . $trace->operation . '</td>
                            <td>' . $trace->date_flt. '</td>
                            <td>' . $trace->show_location . '</td>
                            <td>' . $trace->show_username . '</td>
                        </tr>';
                }
            }

            return Response($output);
        }

        $traces = $query->get();

        return view('traces.search_traces', ['traces' => $traces]);
    }
}
