<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class TracesUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $epc = DB::table('tags')->get();
        $users = DB::table('users')->where('status', '<>', 'ad')->get();
        $locations = DB::table('locations')->get();
        $traces = DB::table('traces')
            ->join('tags', 'traces.epc', '=', 'tags.id')
            ->join('locations', 'traces.location', '=', 'locations.id')
            ->join('users', 'traces.from_user', '=', 'users.id')
            ->select('traces.*',
                DB::raw('tags.id as tags_id,
                    locations.id as location_id,
                    users.id as user_id')
            )
            ->where('traces.id', '=', $id)
            ->get();
        return view('traces.edit_traces', ['traces' => $traces,'epc' => $epc,'users' => $users,'locations' => $locations]);
    }

    public function getDirty(Request $request , $id )
    {
        $traces = DB::table('traces')
            ->select('epc','operation','location','from_user')
            ->where('id', '=', $id)->first();
        $update_traces = $request->all('epc','operation','location','from_user');
        $traces = (array) $traces;
        $array_change = array_diff($traces, $update_traces);
        if(!empty($array_change))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request , $id)
    {
        $request->validate([
            'epc' => 'required',
            'operation' => 'required',
            'location' => 'required',
            'from_user' => 'required'
        ]);

        $epc = $request->epc;
        $operation = $request->operation;
        $location = $request->location;
        $from_user = $request->from_user;

        if($this->getDirty($request, $id))
        {
            DB::table('traces')
                ->where('id', $id)
                ->update(['epc' => $epc ,'operation' => $operation , 'location' => $location , 'from_user' => $from_user]);
            return redirect()->route('traces');
        }
        else
        {
            return redirect()->route('traces.edit', ['id'=>$id])->with('failed',"No fields were modifieds");
        }
    }
}
