<?php

namespace App\Http\Controllers;

use App\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationsUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $locations = DB::table('locations')->where('id', '=', $id)->get();
        return view('locations.edit_locations',['locations'=>$locations]);
    }

    public function getDirty(Request $request , $id )
    {
        $epc = DB::table('locations')
            ->select('location','location_type')
            ->where('id', '=', $id)->first();
        $update_epc = $request->all('location','location_type');
        $epc = (array) $epc;
        $array_change = array_diff($epc, $update_epc);
        if(!empty($array_change))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request , $id)
    {
        $request->validate([
            'location' => 'required|max:255',
            'location_type' => 'required|max:10',
        ]);
        $location = $request->location;
        $location_type = $request->location_type;
        if($this->getDirty($request,$id))
        {
            DB::table('locations')
                ->where('id', $id)
                ->update(['location' => $location ,'location_type' => $location_type ]);
            return redirect()->route('locations');
        }
        else
        {
            return redirect()->route('locations.edit', ['id'=>$id])->with('failed',"No fields were modifieds");
        }
    }
}
