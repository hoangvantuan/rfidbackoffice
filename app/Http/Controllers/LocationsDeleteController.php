<?php

namespace App\Http\Controllers;

use App\Locations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationsDeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $locations = DB::table('locations')->where('id', '=', $id)->get();
        return view('locations.delete_locations',['locations'=>$locations]);
    }

    public function delete(Request $request , $id)
    {
        DB::table('locations')->where('id', '=', $id)->delete();
        return redirect()->route('locations');
    }
}
