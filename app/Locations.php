<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class Locations extends Model
{
    protected $table = 'locations';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location', 'location_type',
    ];

    static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $model->id = Str::uuid();
        });
    }
}
