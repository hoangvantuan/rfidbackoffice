<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Rfid App Backoffice</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('img/faviconnew.png') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

</head>
<body id="page-top">
    <div id="app">
        @include('layouts.left_nav')

        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                @include('layouts.header')

                @yield('content')

            </div>

            @include('layouts.footer')

        </div>

    </div>
</body>
</html>
<div class="load_ajax">
    <img src="{{ asset('img/load.gif') }}" alt="">
</div>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

<script>
    jQuery(document).ready(function($){
        //filter traces
        $('body').on('click','#filter',function(){
            var filter_operation = $('#filter_operation').children("option:selected").val();
            var filter_operation_ts = $('#filter_operation_ts').val()
            $(".load_ajax").show();
            $.ajax({
                type: 'get',
                url: '{{ route('traces.search') }}',
                data: {
                    filter_operation: filter_operation,
                    filter_operation_ts: filter_operation_ts
                },
                beforeSend: function(){
                },
                success:function(data){
                    $(".load_ajax").hide();
                    $('.result_search').html(data);
                }
            });
        });

        //reset filter
        $('body').on('click','#reset',function(){
            $('#filter_operation').val('');
            $('#filter_operation_ts').val('');
            $(".load_ajax").show();
            var filter_operation="";
            var filter_operation_ts="";
            $.ajax({
                type: 'get',
                url: '{{ route('traces.search') }}',
                data: {
                    filter_operation: filter_operation,
                    filter_operation_ts: filter_operation_ts
                },
                beforeSend: function(){
                },
                success:function(data){
                    $(".load_ajax").hide();
                    $('.result_search').html(data);
                }
            });
        });

        //EPC by location
        $('body').on('click','#filter_epc',function(){
            var filter_epc_by_location = $('#filter_epc_by_location').children("option:selected").val();
            $(".load_ajax").show();
            $.ajax({
                type: 'get',
                url: '{{ route('epc-by-location') }}',
                data: {
                    filter_epc_by_location: filter_epc_by_location
                },
                beforeSend: function(){
                },
                success:function(data){
                    $(".load_ajax").hide();
                    $('.result_search').html(data);
                }
            });
        });

        //reset filter
        $('body').on('click','#reset_epc',function(){
            $('#filter_epc_by_location').val('');
            $(".load_ajax").show();
            var filter_epc_by_location="";
            $.ajax({
                type: 'get',
                url: '{{ route('epc-by-location') }}',
                data: {
                    filter_epc_by_location: filter_epc_by_location
                },
                beforeSend: function(){
                },
                success:function(data){
                    $(".load_ajax").hide();
                    $('.result_search').html(data);
                }
            });
        });

        //datepicker
        $("#datepicker" ).datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $("#filter_operation_ts" ).datepicker({
            dateFormat: 'dd-mm-yy'
        });

        //datatable
        $('table.list_users').DataTable();
        $('table.list_locations').DataTable();

        $('table.search_advanced_table').DataTable({
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        $('table.search_advanced_option').DataTable({
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        $('table.table_filter_epc').DataTable({
            "aaSorting": [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        $('table.list_table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        $('table.reports_traces').DataTable({
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
    });
</script>
