<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Rfid App Backoffice {{ now()->year }}</span>
        </div>
    </div>
</footer>
