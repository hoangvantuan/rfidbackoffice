@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit user') }}</div>

                    <div class="card-body">
                        <form action="{{ route('users.edit', ['id'=>$users[0]->id])}}" method = "post" class="create_user">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">User name <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type='text' name='username' class="form-control" autocomplete="off" value="{{$users[0]->username }}" placeholder="User name"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Password <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="password" name='pwd' class="form-control" autocomplete="off" value="{{$users[0]->pwd }}" placeholder="Password"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Status <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select name="status" id=""  class="form-control" value="{{$users[0]->status}}">
                                        <option value="">Status</option>
                                        <option value="act" {{$users[0]->status == "act" ? 'selected' : '' }}>Active</option>
                                        <option value="ina" {{$users[0]->status == "ina" ? 'selected' : '' }}>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <input type ='submit' value = "EDIT USER" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;"/>

                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('failed'))
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    <li>{{ session('failed') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
