@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page-ls">List User</h2>
        <table class="table table-bordered list_users">
            <thead>
            <tr>
                <th>User Name</th>
                <th>Status</th>
                <th>Update ts</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user->username }}</td>
                    <td>{{ $user->status}}</td>
                    <td>{{ $user->update_ts}}</td>
                    <td class="text-center">
                        <a href="{{ route('users.edit', ['id'=>$user->id])}}"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('users.delete', ['id'=>$user->id])}}"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
