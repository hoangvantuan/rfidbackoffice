@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Create audit traces') }}</div>
                    <div class="card-body">
                        <form action="{{ route('traces.store') }}" method = "post" class="create_traces">
                            <input type ="hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label ">Epc <span class="required">*</span></label>
                                    <select name="epc" id="" class="form-control">
                                        <option value="">Epc</option>
                                        @foreach ($epc as $get_epc)
                                            <option value="{{ $get_epc->id }}" {{ old('epc') == $get_epc->id ? 'selected' : '' }}>{{ $get_epc->epc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Operation <span class="required">*</span></label>
                                    <select name="operation" id="" class="form-control">
                                        <option value="">Operation</option>
                                        <option value="IN" {{ old('operation') == 'IN' ? 'selected' : '' }}>IN</option>
                                        <option value="OUT" {{ old('operation') == 'OUT' ? 'selected' : '' }}>OUT</option>
                                        <option value="READ" {{ old('operation') == 'READ' ? 'selected' : '' }}>READ</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Location <span class="required">*</span></label>
                                    <select name="location" id="" class="form-control">
                                        <option value="">Location</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}" {{ old('location') == $location->id ? 'selected' : '' }}>{{ $location->location }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">From users  <span class="required">*</span></label>
                                    <select name="from_user" id="" class="form-control">
                                        <option value="">From users</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}" {{ old('from_user') == $user->id ? 'selected' : '' }}>{{ $user->username }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <input type ='submit' value = "ADD AUDIT TRACES" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;"/>
                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
