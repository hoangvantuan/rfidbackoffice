@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Delete audit traces') }}</div>
                    <div class="card-body">
                        <form action="{{ route('traces.delete',['id'=>$traces[0]->id]) }}" method = "post" class="create_traces">
                            <input type ="hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label ">Epc</label>
                                    <select name="epc" id="" class="form-control" disabled>
                                        <option value="">Epc</option>
                                        @foreach ($epc as $get_epc)
                                            <option value="{{ $get_epc->id }}" {{ $traces[0]->tags_id == $get_epc->id ? 'selected' : '' }}>{{ $get_epc->epc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Operation</label>
                                    <select name="operation" id="" class="form-control" disabled>
                                        <option value="">Operation</option>
                                        <option value="IN" {{ $traces[0]->operation == 'IN' ? 'selected' : '' }}>IN</option>
                                        <option value="OUT" {{ $traces[0]->operation == 'OUT' ? 'selected' : '' }}>OUT</option>
                                        <option value="READ" {{ $traces[0]->operation == 'READ' ? 'selected' : '' }}>READ</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Location</label>
                                    <select name="location" id="" class="form-control" disabled>
                                        <option value="">Location</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}" {{ $traces[0]->location_id == $location->id ? 'selected' : '' }}>{{ $location->location }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">From users</label>
                                    <select name="from_user" id="" class="form-control" disabled>
                                        <option value="">From users</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}" {{ $traces[0]->user_id == $user->id ? 'selected' : '' }}>{{ $user->username }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- Button trigger modal -->
                            <button type="button" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;" data-toggle="modal" data-target="#exampleModal">
                                DELETE AUDIT TRACES
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content model_remove">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Audit Traces</h5>
                                        <p>Are you sure you want to delete this record?</p>
                                        <div class="modal-button">
                                            <button type="submit" class="btn btn-primary yes">Yes</button>
                                            <button type="button" class="btn btn-secondary no" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
