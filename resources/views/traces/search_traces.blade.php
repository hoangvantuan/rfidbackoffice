@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page-ls">Advanced Search</h2>
        <div class="container search_advanced_option">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Option Search</div>
                        <div class="card-body">
                            <div class="row" >
                                <div class="col-md-6">
                                    <label for="">Operation</label>
                                    <select name="operation" id="filter_operation" class="form-control">
                                        <option value="">Operation</option>
                                        <option value="IN">IN</option>
                                        <option value="OUT">OUT</option>
                                        <option value="READ">READ</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Date</label>
                                    <input type="text" name="operation_ts" id="filter_operation_ts" class="form-control"  placeholder="Date" autocomplete="off">
                                </div>
                                <div class="col-md-12 bt_search">
                                    <button type="button" name="filter" id="filter" class="btn submit_custom_color">Filter</button>
                                    <button type="button" name="reset" id="reset" class="btn submit_custom_color">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered search_advanced_table">
            <thead>
            <tr>
                <th>Epc</th>
                <th>Operation</th>
                <th>Operation ts</th>
                <th>Location</th>
                <th>From User</th>
            </tr>
            </thead>
            <tbody class="result_search">
            @foreach ($traces as $trace)
                <tr>
                    <td>{{$trace->show_epc }}</td>
                    <td>{{$trace->operation }}</td>
                    <td>{{$trace->date_flt }}</td>
                    <td>{{$trace->show_location }}</td>
                    <td>{{$trace->show_username }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

