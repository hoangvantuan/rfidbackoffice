@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page-ls">List Audit Traces</h2>
        <table class="table table-bordered list_locations">
            <thead>
            <tr>
                <th>Epc</th>
                <th>Operation</th>
                <th>Operation ts</th>
                <th>Location</th>
                <th>From User</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($traces as $trace)
                <tr>
                    <td>{{$trace->show_epc }}</td>
                    <td>{{$trace->operation }}</td>
                    <td>{{$trace->operation_ts }}</td>
                    <td>{{$trace->show_location }}</td>
                    <td>{{$trace->show_username }}</td>
                    <td class="text-center">
                        <a href="{{route('traces.edit',['id'=>$trace->id])}}"><i class="fas fa-edit"></i></a>
                        <a href="{{route('traces.delete',['id'=>$trace->id])}}"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
