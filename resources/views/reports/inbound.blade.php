@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page">EPC’s Inbound last 15 days</h2>
        <table class="table table-bordered reports_traces">
            <thead>
            <tr>
                <th>Epc</th>
                <th>Operation</th>
                <th>Operation ts</th>
                <th>Location</th>
                <th>From User</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($traces as $trace)
                <tr>
                    <td>{{$trace->show_epc }}</td>
                    <td>{{$trace->operation }}</td>
                    <td>{{$trace->date_flt }}</td>
                    <td>{{$trace->show_location }}</td>
                    <td>{{$trace->show_username }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
