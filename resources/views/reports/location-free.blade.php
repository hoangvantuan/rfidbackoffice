@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page">Free locations</h2>
        <table class="table table-bordered list_table">
            <thead>
            <tr>
                <th>Location</th>
                <th>Location type</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($free_locations as $free_location)
                <tr>
                    <td>{{$free_location->location }}</td>
                    <td>{{$free_location->location_type }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
