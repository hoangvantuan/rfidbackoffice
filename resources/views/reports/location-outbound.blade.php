@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page">Locations with more outbound EPC</h2>
        <table class="table table-bordered list_table">
            <thead>
            <tr>
                <th>Location</th>
                <th>Quantity epc </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($traces as $trace)
                <tr>
                    <td>{{$trace->show_location }}</td>
                    <td>{{$trace->count }} EPC</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
