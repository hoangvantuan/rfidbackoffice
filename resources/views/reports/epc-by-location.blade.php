@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h1>EPC by location</h1>
        <div class="container search_advanced">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">EPC by location</div>
                        <div class="card-body">
                            <div class="row" >
                                <div class="col-md-12">
                                    <label for="">Locations</label>
                                    <select name="operation" id="filter_epc_by_location" class="form-control">
                                        <option value="">Locations</option>
                                        @foreach ($locations_traces as $locations_trace)
                                            <option value="{{$locations_trace->id}}">{{$locations_trace->location }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 bt_search">
                                    <button type="button" name="filter_epc" id="filter_epc" class="btn submit_custom_color">Filter</button>
                                    <button type="button" name="reset_epc" id="reset_epc" class="btn submit_custom_color">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered table_filter_epc">
            <thead>
            <tr>
                <th>Epc</th>
                <th>Operation</th>
                <th>Operation ts</th>
                <th>Location</th>
                <th>From User</th>
            </tr>
            </thead>
            <tbody class="result_search">
            @foreach ($traces as $trace)
                <tr>
                    <td>{{$trace->show_epc }}</td>
                    <td>{{$trace->operation }}</td>
                    <td>{{$trace->operation_ts }}</td>
                    <td>{{$trace->show_location }}</td>
                    <td>{{$trace->show_username }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
