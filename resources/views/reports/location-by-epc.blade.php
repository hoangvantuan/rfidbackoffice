@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page">Locations by epc</h2>
        <table class="table table-bordered list_table">
            <thead>
            <tr>
                <th>Location</th>
                <th>Location type</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($get_locations as $get_location)
                <tr>
                    <td>{{$get_location->location }}</td>
                    <td>{{$get_location->location_type }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
