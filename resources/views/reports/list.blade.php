@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2 class="col-md-12 title-page">Reports</h2>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('inbound')}}">EPC’s Inbound last 15 days</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('outbound')}}">EPC’s Outbound last 15 days</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('location-inbound')}}">Locations with more inbound EPC</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('location-outbound')}}">Locations with more outbound EPC</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('epc-by-location')}}">EPC by location</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('location-by-epc')}}">Locations by EPC</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card box-reports">
                    <div class="card-body text-center">
                        <a href="{{route('free-location')}}">Free locations</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
