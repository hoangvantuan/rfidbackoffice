@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Delete locations') }}</div>

                    <div class="card-body">
                        <form action="{{ route('locations.delete', ['id'=>$locations[0]->id])}}" method = "post" class="create_user">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Locations</label>
                                <div class="col-md-6">
                                    <input type='text' name='location' class="form-control" autocomplete="off" value="{{$locations[0]->location}}" disabled/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Locations type</label>
                                <div class="col-md-6">
                                    <input type="text" name='location_type' class="form-control" autocomplete="off" value="{{$locations[0]->location_type}}" disabled/>
                                </div>
                            </div>
                            <!-- Button trigger modal -->
                            <button type="button" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;" data-toggle="modal" data-target="#exampleModal">
                                DELETE LOCATIONS
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content model_remove">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Locations</h5>
                                        <p>Are you sure you want to delete this record?</p>
                                        <div class="modal-button">
                                            <button type="submit" class="btn btn-primary yes">Yes</button>
                                            <button type="button" class="btn btn-secondary no" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
