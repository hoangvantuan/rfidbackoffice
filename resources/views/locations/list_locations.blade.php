@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page-ls">List Locations</h2>
        <table class="table table-bordered list_locations">
            <thead>
            <tr>
                <th>Location</th>
                <th>Location type</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($locations as $location)
                <tr>
                    <td>{{$location->location }}</td>
                    <td>{{ $location->location_type}}</td>
                    <td class="text-center">
                        <a href="{{ route('locations.edit', ['id'=>$location->id])}}"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('locations.delete', ['id'=>$location->id])}}"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
