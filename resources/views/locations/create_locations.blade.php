@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create locations') }}</div>

                    <div class="card-body">
                        <form action="{{ route('locations.store') }}" method = "post" class="create_user">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Location <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type='text' name='location' class="form-control" autocomplete="off" value="{{old('location')}}" placeholder="Location"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Location type <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name='location_type' class="form-control" autocomplete="off" value="{{old('location_type')}}" placeholder="Location type"/>
                                </div>
                            </div>
                            <input type ='submit' value = "ADD LOCATIONS" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;"/>

                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
