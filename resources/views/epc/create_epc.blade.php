@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Create epc') }}</div>
                    <div class="card-body">
                        <form action="{{ route('epc.store') }}" method = "post" class="create_epc">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label ">Epc <span class="required">*</span></label>
                                    <input type="text" name='epc' class="form-control" autocomplete="off" value="{{old('epc')}}" placeholder="Epc"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Component <span class="required">*</span></label>
                                    <input type="text" name='component' class="form-control" autocomplete="off" value="{{old('component')}}" placeholder="Component"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Product nr <span class="required">*</span></label>
                                    <input type="text" name='product_nr' class="form-control" autocomplete="off" value="{{old('product_nr')}}" placeholder="Product nr"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Manufacturing <span class="required">*</span></label>
                                    <input type="text" name='manufacturing' class="form-control" autocomplete="off"  id="datepicker" value="{{old('manufacturing')}}" placeholder="Manufacturing"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Lot number <span class="required">*</span></label>
                                    <input type="number" name='lot_number' class="form-control" autocomplete="off" value="{{old('lot_number')}}" placeholder="Lot number"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Compound <span class="required">*</span></label>
                                    <input type="text" name='compound' class="form-control" autocomplete="off" value="{{old('compound')}}" placeholder="Compound"/>
                                </div>
                                <div class="col-md-12 mr_form">
                                    <label class="col-form-label">Notes <span class="required">*</span></label>
                                    <textarea name="notes" id="" cols="30" rows="3" class="form-control" value="" placeholder="Notes">{{old('notes')}}</textarea>
                                </div>
                            </div>
                            <input type ='submit' value = "ADD EPC" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;"/>
                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
