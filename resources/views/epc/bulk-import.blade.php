@extends('layouts.app')
@section('content')
<div class="container">

    <h3>File Example</h3>
    <a href="{{ asset('uploads/tags.csv') }}" class="file-example">Download</a>

    <div class="card bg-light mt-3">
        <div class="card-header">
            Bulk Import
        </div>
        <div class="card-body">

            <form action="{{ route('epc.bulk-import') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="file" class="form-control bulk" >
                <br>
                <button class="btn submit_custom_color">Import Bulk Data</button>
            </form>

            @if ($errors->any())
                <div class="alert alert-danger messenger-error">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
