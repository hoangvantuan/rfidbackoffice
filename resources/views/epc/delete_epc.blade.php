@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Delete epc') }}</div>
                    <div class="card-body">
                        <form action="{{ route('epc.delete',['id'=>$epc[0]->id]) }}" method = "post" class="delete_epc">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <div class="col-md-6 mr_form" style="margin: 0 auto;margin-bottom: 15px;">
                                    <label class="col-form-label ">Epc</label>
                                    <input type="text" name='epc' class="form-control" autocomplete="off" value="{{$epc[0]->epc}}" disabled/>
                                </div>
                            </div>
                            <!-- Button trigger modal -->
                            <button type="button" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;" data-toggle="modal" data-target="#exampleModal">
                                DELETE EPC
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content model_remove">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Epc</h5>
                                        <p>Are you sure you want to delete this record?</p>
                                        <div class="modal-button">
                                            <button type="submit" class="btn btn-primary yes">Yes</button>
                                            <button type="button" class="btn btn-secondary no" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
