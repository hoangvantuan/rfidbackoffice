@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Edit epc') }}</div>
                    <div class="card-body">
                        <form action="{{ route('epc.edit', ['id'=>$epc[0]->id])}}" method = "post" class="edit_epc">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="form-group row">
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label ">Epc <span class="required">*</span></label>
                                    <input type="text" name='epc' class="form-control" autocomplete="off" value="{{$epc[0]->epc}}" placeholder="Epc"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Component <span class="required">*</span></label>
                                    <input type="text" name='component' class="form-control" autocomplete="off" value="{{$epc[0]->component}}" placeholder="Component"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Product nr <span class="required">*</span></label>
                                    <input type="text" name='product_nr' class="form-control" autocomplete="off" value="{{$epc[0]->product_nr}}" placeholder="Product nr"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Manufacturing <span class="required">*</span></label>
                                    <input type="text" name='manufacturing' class="form-control" autocomplete="off"  id="datepicker" value="{{date('d-m-Y', strtotime($epc[0]->manufacturing))}}" placeholder="Manufacturing"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Lot number <span class="required">*</span></label>
                                    <input type="number" name='lot_number' class="form-control" autocomplete="off" value="{{$epc[0]->lot_number}}" placeholder="Lot number"/>
                                </div>
                                <div class="col-md-6 mr_form">
                                    <label class="col-form-label">Compound <span class="required">*</span></label>
                                    <input type="text" name='compound' class="form-control" autocomplete="off" value="{{$epc[0]->compound}}" placeholder="Compound"/>
                                </div>
                                <div class="col-md-12 mr_form">
                                    <label class="col-form-label">Notes <span class="required">*</span></label>
                                    <textarea name="notes" id="" cols="30" rows="3" class="form-control" value="" placeholder="Notes">{{$epc[0]->notes}}</textarea>
                                </div>
                            </div>
                            <input type ='submit' value = "EDIT EPC" class="form-control submit_custom_color" style="width: max-content;margin: 0 auto;"/>
                        </form>
                        @if ($errors->any())
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('failed'))
                            <div class="alert alert-danger messenger-error">
                                <ul>
                                    <li>{{ session('failed') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
