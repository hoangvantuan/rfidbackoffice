@extends('layouts.app')

@section('content')
    <div class="col-md-12 content_table">
        <h2 class="title-page-ls">List Epc</h2>
        <table class="table table-bordered list_locations">
            <thead>
            <tr>
                <th>Epc</th>
                <th>Component</th>
                <th>Product nr</th>
                <th>Manufacturing</th>
                <th>Lot number</th>
                <th>Compound</th>
                <th>Notes</th>
                <th>Update ts</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($epc as $epc_get)
                <tr>
                    <td>{{$epc_get->epc }}</td>
                    <td>{{ $epc_get->component}}</td>
                    <td>{{ $epc_get->product_nr }}</td>
                    <td>{{date('d-m-Y', strtotime($epc_get->manufacturing))}}</td>
                    <td>{{ $epc_get->lot_number }}</td>
                    <td>{{ $epc_get->compound }}</td>
                    <td>{{ $epc_get->notes }}</td>
                    <td>{{ $epc_get->update_ts }}</td>
                    <td class="text-center">
                        <a href="{{route('epc.edit', ['id'=>$epc_get->id])}}"><i class="fas fa-edit"></i></a>
                        <a href="{{route('epc.delete', ['id'=>$epc_get->id])}}"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
