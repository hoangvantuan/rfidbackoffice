<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
})->name('home');;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'RfidUser@index')->name('users');
    Route::get('/create', 'RfidUser@create')->name('users.create');
    Route::post('/store', 'RfidUser@store')->name('users.store');
    Route::get('/edit/{id}', 'RfidUserUpdateController@show')->name('users.edit');
    Route::post('/edit/{id}', 'RfidUserUpdateController@edit')->name('users.edit');
    Route::get('/delete/{id}', 'RfidUserDeleteController@show')->name('users.delete');
    Route::post('/delete/{id}', 'RfidUserDeleteController@delete')->name('users.delete');
});

Route::group(['prefix' => 'locations'], function () {
    Route::get('/', 'LocationsController@index')->name('locations');
    Route::get('/create', 'LocationsController@create')->name('locations.create');
    Route::post('/store', 'LocationsController@store')->name('locations.store');
    Route::get('/edit/{id}', 'LocationsUpdateController@show')->name('locations.edit');
    Route::post('/edit/{id}', 'LocationsUpdateController@edit')->name('locations.edit');
    Route::get('/delete/{id}', 'LocationsDeleteController@show')->name('locations.delete');
    Route::post('/delete/{id}', 'LocationsDeleteController@delete')->name('locations.delete');
});

Route::group(['prefix' => 'epc'], function () {
    Route::get('/', 'EpcController@index')->name('epc');
    Route::get('/create', 'EpcController@create')->name('epc.create');
    Route::post('/store', 'EpcController@store')->name('epc.store');
    Route::get('/edit/{id}', 'EpcUpdateController@show')->name('epc.edit');
    Route::post('/edit/{id}', 'EpcUpdateController@edit')->name('epc.edit');
    Route::get('/delete/{id}', 'EpcDeleteController@show')->name('epc.delete');
    Route::post('/delete/{id}', 'EpcDeleteController@delete')->name('epc.delete');
    Route::get('/bulk-import', 'ImportExportController@importExportView')->name('epc.bulk-import');
    Route::post('/bulk-import', 'ImportExportController@import')->name('epc.bulk-import');
});

Route::group(['prefix' => 'traces'], function () {
    Route::get('/', 'TracesController@index')->name('traces');
    //Route::get('/create', 'TracesController@create')->name('traces.create');
    Route::post('/store', 'TracesController@store')->name('traces.store');
    Route::get('/edit/{id}', 'TracesUpdateController@show')->name('traces.edit');
    Route::post('/edit/{id}', 'TracesUpdateController@edit')->name('traces.edit');
    Route::get('/delete/{id}', 'TracesDeleteController@show')->name('traces.delete');
    Route::post('/delete/{id}', 'TracesDeleteController@delete')->name('traces.delete');
    Route::get('/search', 'TracesSearchController@search')->name('traces.search');
});

Route::group(['prefix' => 'reports'], function () {
    Route::get('/', 'ReportsController@index')->name('reports');
    Route::get('/inbound', 'ReportsController@inbound')->name('inbound');
    Route::get('/outbound', 'ReportsController@outbound')->name('outbound');
    Route::get('/location-inbound', 'ReportsController@LocationInbound')->name('location-inbound');
    Route::get('/location-outbound', 'ReportsController@LocationOutbound')->name('location-outbound');
    Route::get('/epc-by-location', 'EpcByLocationController@EpcByLocation')->name('epc-by-location');
    Route::get('/location-by-epc', 'LocationByEpcController@LocationByEpc')->name('location-by-epc');
    Route::get('/free-location', 'LocationFreeController@FreeLocation')->name('free-location');
});
